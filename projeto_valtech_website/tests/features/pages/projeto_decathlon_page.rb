class User < SitePrism::Page
    set_url 'https://www.valtech.com/pt-br/'

    element :recusar_cookie, '#CybotCookiebotDialogBodyButtonRefuse'
    element :banner, '.site-nav__menu__list'
    element :projetos, 'a[href="/pt-br/projetos/"]'
    element :decathlon, 'a[href="/pt-br/projetos/decathlon/"]'
    
    def clicar_recusar_cookie
        recusar_cookie.click
    end
    
    def passar_mouse
        banner.hover
    end

    def clicar_projetos
        projetos.click
    end

    def clicar_decathlon
        decathlon.click
    end
    
end