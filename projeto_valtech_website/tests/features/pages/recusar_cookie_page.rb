class User < SitePrism::Page
    set_url 'https://www.valtech.com/pt-br/'
    
    element :recusar_cookie, '#CybotCookiebotDialogBodyButtonRefuse'
    
    
    def clicar_recusar_cookie
        recusar_cookie.click
    end
end