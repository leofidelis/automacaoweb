class User < SitePrism::Page
    set_url 'https://www.valtech.com/pt-br/'
    element :banner, '.site-nav__menu__list'

    def passar_mouse
        banner.hover
    end
end
