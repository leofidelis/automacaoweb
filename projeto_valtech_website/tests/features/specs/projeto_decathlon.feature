#language: pt

@projeto_decathlon
Funcionalidade: Verificar direcionamento da página inicial até o projeto da decathlon

Cenario: Entrar no projeto da decathlon
Quando Entro na página inicial da Valtech e clico em projetos
E clico em decathlon
Entao espero carregar a página da decathlon