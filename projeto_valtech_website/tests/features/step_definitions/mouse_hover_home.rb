Quando('eu passo o mouse no banner da home') do
    user.load
    user.passar_mouse
end

Entao('verifico se o banner interagiu') do
    @mensagem = find('.site-nav__menu__title.js-accordion-button')
    expect(@mensagem.text).to eq 'nosso trabalho'
end