Quando('Entro na página inicial da Valtech e clico em projetos') do
    user.load
    sleep(1)
    user.clicar_recusar_cookie
    sleep(1)
    user.passar_mouse
    sleep(1)
    user.clicar_projetos
    sleep(1)
end
   
E('clico em decathlon') do
    user.clicar_decathlon
    sleep(1)
end
  
Entao('espero carregar a página da decathlon') do
    @mensagem = find('.masthead-secondary__lead')
    expect(@mensagem.text).to eq 'Novos aplicativos de tecnologia de RV melhoram as receitas e a experiência do usuário'
end