require 'capybara/cucumber'                     ##Desativar quando utilizar o end_to_end
#require 'capybara'                                  ##############################################
#require 'capybara/dsl'                            ##Esses pacotes que estao comentados serve para o end_to_end
#require 'capybara/rspec/matchers'
require  'selenium-webdriver'

#World(Capybara::DSL)                             ######################################
#World(Capybara::RSpecMatchers)                    ##Ativar para usar o end_to_end

Capybara.configure do |config|
    #selenium selenium_chrome selenium_chrome_healess
    config.default_driver = :selenium_chrome
    config.app_host = 'https://automacaocombatista.herokuapp.com'
    config.default_max_wait_time = 5
end
