Quando('Interajo com dropdown e select') do
    visit '/buscaelementos/dropdowneselect'
    find('.btn.dropdown-button').click
    find('#dropdown3').click
    find('option[value="2"]').select_option
    sleep(2)
    select 'Chrome', from: 'dropdown'
    sleep(2)
end

Quando('preencho o autocomplete') do
    visit '/widgets/autocomplete'
    find('#autocomplete-input').set 'Per'
    find('ul', text: 'Pernambuco').click
    sleep(4)
end