Quando('eu faço um upload de arquivo') do
    visit '/outros/uploaddearquivos'
    #attach_file('upload', 'C:\Users\leonardo.fidelis\Documents\cursoruby\capybara_test\tests\features\logo.png', make_visible: true)`
    
    #Esta opção é melhor do que colocar todo o endereço
    @foto = File.join(Dir.pwd, 'features/logo.png')
    attach_file('upload', @foto, make_visible: true)
end