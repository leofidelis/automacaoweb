Quando('eu cadastro o usuario') do
 visit 'users/new'
fill_in(id: 'user_name', with: 'Leonardo')
find(id:'user_lastname').set('Fidelis')
find('#user_email').send_keys('leofidelis11@hotmail.com')
find('input[value="Criar"]').click
end

Entao('verifico se o usuario foi cadastrado') do
    texto = find('#notice')
    expect(texto.text).to eq 'Usuário Criado com sucesso'
end

Quando('eu edito um usuario') do
    sleep(3)
    find('.btn.waves-light.blue').click
    sleep(2)
end

Entao('verifico se o usuario foi editado') do

end