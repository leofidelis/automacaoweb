Quando('eu marco um radiobox e checkbox') do
    visit '/buscaelementos/radioecheckbox'
    find('label[for="white"]').click
    check('purple', allow_label_click: true)
    sleep(1)
    uncheck('purple', allow_label_click: true)
    sleep(1)
    find('label[for="blue"]').click
    sleep(1)
    choose('yellow', allow_label_click: true)
    sleep(1)
end