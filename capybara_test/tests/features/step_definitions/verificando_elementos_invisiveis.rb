Quando('eu clico em um botao') do
    visit '/buscaelementos/botoes'
    find('#teste').click  
    find('#teste').click    #Aqui repete o comando pra configurar um segundo click
end

Entao('verifico se o texto desapareceu da tela com sucesso') do
    has_no_text?('Você Clicou no Botão!')  #esse comando verifica se NÂO tem o texto
end