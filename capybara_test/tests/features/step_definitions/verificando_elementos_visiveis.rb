Quando('clico no botao') do
    visit '/buscaelementos/botoes'
    find('#teste').click
end

Entao('verifico se o texto apareceu na tela com sucesso') do
   # @texto = find('#div1')
   # expect(@texto.text).to eq 'Você Clicou no Botão!'  ##Esses três comandos realizam a mesma função;
     page.has_text?('Você Clicou no Botão!')            ##buscam um texto na tela;
     have_text('Você Clicou no Botão!')                 ##pode usar qualquer um.
end