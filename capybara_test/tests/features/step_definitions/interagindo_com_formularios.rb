Quando('eu faco cadastro') do
    visit '/users/new'
    fill_in(id: 'user_name', with: 'Leonardo')
    find(id:'user_lastname').set('Fidelis')
    find('#user_email').send_keys('leofidelis11@hotmail.com')
    fill_in(id: 'user_address', with: 'Rua Napoleão Gregório Lobo, 100')
    find(id:'user_university').set('UFPE')
    find('#user_profile').send_keys('QA Trainee') #tanto faz utilizar id: ou # para identificar id
    find(id:'user_gender').set('Masculino')
    find('#user_age').send_keys('25')
    find('input[value="Criar"]').click
end

Entao('verifico se fui cadastrado') do
    texto = find('#notice')
    expect(texto.text).to eq 'Usuário Criado com sucesso'
end