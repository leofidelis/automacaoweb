Quando('eu divido doi numeros {int} \/ {int}') do |valor1, valor2|
    @divisao = valor1 / valor2
end

Entao('o resultado da divisão') do
    expect(@divisao).to eq 5
end

Quando('eu multiplico dois numeros {int} * {int}') do |valor3, valor4|
    @multiplicacao = valor3 * valor4
end

Entao('o resultado da multiplicação') do
    expect(@multiplicacao).to eq 20
end