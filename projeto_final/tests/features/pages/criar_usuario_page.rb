class User < SitePrism::Page
    set_url '/users/new'
    element :nome, "#user_name"
    element :ultimo_nome, "#user_lastname"
    element :email, "#user_email"
    element :endereco, "#user_address"
    element :universidade, "#user_university"
    element :profissao, "#user_profile"
    element :genero, "#user_gender"
    element :idade, "#user_age"

    element :criar, 'input[value="Criar"]'

    def preencher_usuario
        nome.set 'Joao'
        ultimo_nome.set 'Medeiros'
        email.set 'joao123@gmail.com'
        endereco.set "Rua da Solidao, 164"
        universidade.set 'UFSC'
        profissao.set 'Quality Assurance'
        genero.set 'Masculino'
        idade.set '28'
        criar.click
    end
end