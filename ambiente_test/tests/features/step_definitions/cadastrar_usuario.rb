Quando('eu cadastro o usuario') do
    home.load
    home.preencher
    sleep(1)
    home.botaocriar.click
end

Entao('verifico se o usuario foi cadastrado com sucesso') do
    texto = find('#notice')
  expect(texto.text).to eq 'Usuário Criado com sucesso'
end