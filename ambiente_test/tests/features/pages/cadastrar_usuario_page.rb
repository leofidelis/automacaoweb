class CadastrarUsuario < SitePrism::Page
    set_url '/users/new'

    element :nome, '#user_name'
    element :ultimo_nome, '#user_lastname'
    element :email, '#user_email'
    element :botaocriar, 'input[value="Criar"]'

    def preencher
        nome.set 'Leo'
        ultimo_nome.set 'Fidelis'
        email.set '123deoliveira4@gmail.com'
    end
end

