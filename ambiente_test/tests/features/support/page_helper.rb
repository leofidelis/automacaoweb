Dir[File.join(File.dirname(__FILE__), '../pages/*_page.rb')].each { |file| require file }

module Pages

    def home
        @home ||= CadastrarUsuario.new
    end
end